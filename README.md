# IPIP Multiqueue Optimizer
IPIP Multiqueue Optimizer is a tc action designed to work on top of the clsact qdisc on the egress side of a NIC. Its primary function is to randomize the source IP address within the outer IP/IPv6 header. This assists NICs in efficently directing traffic into multiple RX queues.
It's written to use in combination with [IPVS with IPIP encapsulation](http://www.linuxvirtualserver.org/VS-IPTunneling.html)
## Development
A tiny Makefile is provided to take care of `go generate` and `go build` tasks.
### Debian packaging
The current eBPF code can be loaded by a bullseye (5.10) kernel as well as a bookworm (6.1) kernel but it needs to be built on bookworm (golang-github-cilium-ebpf-dev version provided in bullseye is too old)
## How to use it
IPIP Multiqueue Optimizer ships a Go wrapper powered by [Cilium eBPF framework](https://github.com/cilium/ebpf) that takes care of compiling the eBPF program and loading the TC filter as long as the clsact qdisc is already present on the target interface(s). It will attach the eBPF optimizer to any network device passed as an argument that has the clsact qdisc already installed
```
# ipip-multiqueue-optimizer lo enp89s0
```
### Prometheus endpoint
A Prometheus endpoint is available if `ipip-multiqueue-optimizer` is started using the `-p` flag:
```
# ipip-multiqueue-optimizer -p :8888 lo enp89s0
$ curl -s 127.0.0.1:8888/metrics |grep optimizer
# HELP ipip_multiqueue_optimizer_packets_total Number of packets per interface and state
# TYPE ipip_multiqueue_optimizer_packets_total counter
ipip_multiqueue_optimizer_packets_total{interface="enp89s0",state="processed"} 0
ipip_multiqueue_optimizer_packets_total{interface="enp89s0",state="skipped"} 363
ipip_multiqueue_optimizer_packets_total{interface="lo",state="processed"} 0
ipip_multiqueue_optimizer_packets_total{interface="lo",state="skipped"} 67
# HELP ipip_multiqueue_optimizer_up Reports if the current scrape was successful or not
# TYPE ipip_multiqueue_optimizer_up counter
ipip_multiqueue_optimizer_up{interface="enp89s0"} 1
ipip_multiqueue_optimizer_up{interface="lo"} 1

```
## How to use the eBPF program
### Dependencies
* llvm
* clang
* linux-headers
* libbpf-dev
* gcc-multilib
### Compilation
Code can be compiled with the following cmd:
```
clang -O2 -emit-llvm -c ipip-mq-optimizer.c -o - | llc-14 -march=bpf -filetype=obj -o ipip-mq-optimizer.o
```
### Installation
```
tc qdisc add dev eth1 clsact
tc filter add dev eth1 egress bpf direct-action object-file ipip-mq-optimizer.o section tc verbose
```
### Debugging
[pwru (packet, where are you?)](https://github.com/cilium/pwru) is a great tool to debug packet manipulation performed with eBPF and the networking stack in general:
```
$ pwru --filter-ifname eth1 --output-tuple "dst host (192.168.42.100 or 10.10.10.10)"
[...]
# Incoming packet from a client (192.168.42.1) towards a VIP (10.10.10.10:80)
0xffff9c9cc4c4e100      0           [pwru]         inet_gro_receive 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]         tcp4_gro_receive 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]          tcp_gro_receive 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]   skb_defer_rx_timestamp 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]              ip_rcv_core 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]       tcp_v4_early_demux 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]     ip_route_input_noref 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]      ip_route_input_slow 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]      fib_validate_source 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]         ip_local_deliver 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]             nf_hook_slow 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]      skb_ensure_writable 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru] iptunnel_handle_offloads 192.168.42.1:37102->10.10.10.10:80(tcp)
0xffff9c9cc4c4e100      0           [pwru]                 skb_push 192.168.42.1:37102->10.10.10.10:80(tcp)
# IPVS targets real server 192.168.42.100 via IPIP tunneling
0xffff9c9cc4c4e100      0           [pwru]             tcf_classify 192.168.42.42:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]      skb_ensure_writable 192.168.42.42:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]      skb_ensure_writable 192.168.42.42:0->192.168.42.100:0()
# ipip-multiqueue-optimizer replaces the source IP of the outer IPIP packet
0xffff9c9cc4c4e100      0           [pwru]      netdev_core_pick_tx 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]          sch_direct_xmit 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]   validate_xmit_skb_list 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]        validate_xmit_skb 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]       netif_skb_features 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]  passthru_features_check 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]     skb_network_protocol 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]  skb_csum_hwoffload_help 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]        skb_checksum_help 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]      skb_ensure_writable 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]       validate_xmit_xfrm 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]      dev_hard_start_xmit 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]   skb_clone_tx_timestamp 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]             skb_to_sgvec 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]           __skb_to_sgvec 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]         napi_consume_skb 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]   skb_release_head_state 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]         skb_release_data 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]            skb_free_head 172.16.186.196:0->192.168.42.100:0()
0xffff9c9cc4c4e100      0           [pwru]       napi_skb_cache_put 172.16.186.196:0->192.168.42.100:0()
```
## Caveats
IPIP Multiqueue Optimizer assumes that all outgoing IPIP / IP6tnl traffic needs to be optimized
