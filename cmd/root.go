/*
Copyright © 2023 Valentin Gutierrez

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
package cmd

import (
	"context"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/mmatczuk/anyflag"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/repos/sre/ipip-multiqueue-optimizer/pkg/metrics"
	"gitlab.wikimedia.org/repos/sre/ipip-multiqueue-optimizer/pkg/optimizer"
)

var (
	ifaces               []*net.Interface
	PrometheusBindAdress string
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use: "ipip-multiqueue-optimizer",
	Run: func(cmd *cobra.Command, args []string) {
		var opMap map[string]*optimizer.Optimizer
		opMap = make(map[string]*optimizer.Optimizer)

		for _, iface := range ifaces {
			opMap[iface.Name] = optimizer.NewOptimizer()
			if err := opMap[iface.Name].Init(); err != nil {
				log.Fatalf("Unable to init eBPF program: %v", err)
			}
			defer opMap[iface.Name].Close()

			if err := opMap[iface.Name].AttachToInterface(iface); err != nil {
				log.Printf("Unable to attach to %v: %v", iface.Name, err)
			}
		}

		ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
		defer stop()

		if PrometheusBindAdress != "" {
			reg := prometheus.NewPedanticRegistry()
			reg.MustRegister(
				&metrics.OptimizerMetricCollector{Programs: opMap},
				// Add the standard process and Go metrics to the custom registry
				prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{}),
				prometheus.NewGoCollector(),
			)
			go func() {
				http.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
				log.Fatal(http.ListenAndServe(PrometheusBindAdress, nil))
			}()
		}

		for {
			select {
			case <-ctx.Done():
				log.Println("Exiting")
				return
			}
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.Flags().StringVarP(&PrometheusBindAdress, "prometheus", "p", "", "Prometheus bind address")
	interfaceSlice := anyflag.NewSliceValue[*net.Interface](nil, &ifaces, net.InterfaceByName)
	rootCmd.Flags().VarP(interfaceSlice, "interface", "i", "interfaces where IPIP MQ optimization will happen [can be specified multiple times]")

	rootCmd.MarkFlagRequired("interface")
}
