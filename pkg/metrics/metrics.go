package metrics

import (
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.wikimedia.org/repos/sre/ipip-multiqueue-optimizer/pkg/optimizer"
)

const (
	SKIPPED_LABEL   = "skipped"
	PROCESSED_LABEL = "processed"
)

var (
	PacketsDesc = prometheus.NewDesc(
		"ipip_multiqueue_optimizer_packets_total",
		"Number of packets per interface and state",
		[]string{"interface", "state"}, nil,
	)
	ScraperUpDesc = prometheus.NewDesc(
		"ipip_multiqueue_optimizer_up",
		"Reports if the current scrape was successful or not",
		[]string{"interface"}, nil,
	)
)

type OptimizerMetricCollector struct {
	Programs map[string]*optimizer.Optimizer
}

func (omc OptimizerMetricCollector) Describe(ch chan<- *prometheus.Desc) {
	prometheus.DescribeByCollect(omc, ch)
}

func (omc OptimizerMetricCollector) Collect(ch chan<- prometheus.Metric) {
	for ifaceName, program := range omc.Programs {
		successful := float64(1)
		stats, err := program.GetStats()
		if err != nil {
			successful = float64(0)
		}

		ch <- prometheus.MustNewConstMetric(
			ScraperUpDesc,
			prometheus.CounterValue,
			successful,
			ifaceName)

		if err != nil {
			continue
		}

		ch <- prometheus.MustNewConstMetric(
			PacketsDesc,
			prometheus.CounterValue,
			float64(stats.PcktsSkipped),
			ifaceName, SKIPPED_LABEL)

		ch <- prometheus.MustNewConstMetric(
			PacketsDesc,
			prometheus.CounterValue,
			float64(stats.PcktsProcessed),
			ifaceName, PROCESSED_LABEL)
	}
}
