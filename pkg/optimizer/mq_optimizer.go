package optimizer

import (
	"errors"
	"fmt"
	"log"
	"net"

	"github.com/florianl/go-tc"
	"github.com/florianl/go-tc/core"
	"github.com/mdlayher/netlink"
	"golang.org/x/sys/unix"
)

//go:generate go run github.com/cilium/ebpf/cmd/bpf2go -cc clang -cflags -O2 optimizer ipip-mq-optimizer.c

var (
	ErrorBPFNotLoaded         = errors.New("BPF program not loaded")
	ErrorOptimizerNotAttached = errors.New("not attached to any interface")
)

type Optimizer struct {
	objs      optimizerObjects
	iface     *net.Interface
	loaded    bool
	attached  bool
	programFD int
	bpfName   string
	tcnl      *tc.Tc
}

func NewOptimizer() *Optimizer {
	return &Optimizer{
		objs:     optimizerObjects{},
		loaded:   false,
		attached: false,
	}
}

func (op *Optimizer) Init() error {
	// load eBPF program
	if err := loadOptimizerObjects(&op.objs, nil); err != nil {
		log.Printf("Unable to load eBPF program %v: %v\n", op.objs, err)
		return err
	}
	op.programFD = op.objs.IpipOptimizer.FD()
	op.loaded = true

	return nil
}

func (op *Optimizer) AttachToInterface(iface *net.Interface) error {
	if !op.loaded {
		return ErrorBPFNotLoaded
	}

	if op.attached {
		return fmt.Errorf("already attached to %v", op.iface.Name)
	}

	if !isClsactPresent(*iface) {
		return fmt.Errorf("clsact qdisc not found on %v", iface.Name)
	}
	op.iface = iface

	var err error
	op.tcnl, err = tc.Open(&tc.Config{})
	if err != nil {
		log.Printf("could not open rtnetlink socket: %v\n", err)
		return err
	}

	fd := uint32(op.programFD)
	flags := uint32(0x1)
	op.bpfName = fmt.Sprintf("ipip-mq-optimizer-%v", iface.Name)

	filter := tc.Object{
		tc.Msg{
			Family:  unix.AF_UNSPEC,
			Ifindex: uint32(op.iface.Index),
			Handle:  0,
			Parent:  core.BuildHandle(tc.HandleRoot, tc.HandleMinEgress),
			Info:    0x300,
		},
		tc.Attribute{
			Kind: "bpf",
			BPF: &tc.Bpf{
				FD:    &fd,
				Flags: &flags,
				Name:  &op.bpfName,
			},
		},
	}

	log.Printf("Attaching IPIP Multiqueue Optimizer on %v", iface.Name)
	if err := op.tcnl.Filter().Add(&filter); err != nil {
		log.Printf("could not attach filter for eBPF program: %v\n", err)
		op.tcnl.Close()
		return err
	}

	op.attached = true
	return nil
}

// Fetch eBPF program stats
func (op *Optimizer) GetStats() (optimizerOpStats, error) {
	var stats optimizerOpStats

	if !op.attached {
		return stats, ErrorOptimizerNotAttached
	}

	mapKey := uint32(0)

	var value []optimizerOpStats

	if err := op.objs.OpStatsMap.Lookup(mapKey, &value); err != nil {
		return stats, err
	}

	// opStatsMap is a per cpu array (BPF_MAP_TYPE_PERCPU_ARRAY)
	for _, cpuStats := range value {
		stats.PcktsProcessed += cpuStats.PcktsProcessed
		stats.PcktsSkipped += cpuStats.PcktsSkipped
	}

	return stats, nil
}

func (op *Optimizer) Detach() error {
	if !op.attached {
		return ErrorOptimizerNotAttached
	}

	msg := tc.Msg{
		Family:  unix.AF_UNSPEC,
		Ifindex: uint32(op.iface.Index),
		Handle:  0,
		Parent:  core.BuildHandle(tc.HandleRoot, tc.HandleMinEgress),
	}

	filters, err := op.tcnl.Filter().Get(&msg)
	if err != nil {
		log.Printf("Unable to fetch filters: %v\n", err)
		return err
	}

	removed := false
	for _, filter := range filters {
		if filter.Attribute.Kind == "bpf" && filter.Attribute.BPF != nil {
			if *filter.Attribute.BPF.Name == op.bpfName {
				log.Printf("Detaching IPIP Multiqueue Optimizer from %v", op.iface.Name)
				if err := op.tcnl.Filter().Delete(&filter); err != nil {
					return err
				}
				removed = true
			}
		}
	}
	if !removed {
		return fmt.Errorf("Unable to find TC BPF filter %v on interface %v\n", op.bpfName, op.iface.Name)
	}

	op.iface = nil
	op.attached = false
	return nil
}

func (op *Optimizer) Close() {
	if op.attached {
		op.Detach()
		op.tcnl.Close()
	}

	if op.loaded {
		op.objs.Close()
	}
}

// Check if clsact (https://lwn.net/Articles/671458/) is present
// on a specific interface
func isClsactPresent(netInterface net.Interface) bool {
	// open a rtnetlink socket
	rtnl, err := tc.Open(&tc.Config{})
	if err != nil {
		log.Println("could not open rtnetlink socket: %v\n", err)
		return false
	}
	defer func() {
		if err := rtnl.Close(); err != nil {
			log.Printf("could not close rtnetlink socket: %v\n", err)
		}
	}()

	// For enhanced error messages from the kernel, it is recommended to set
	// option `NETLINK_EXT_ACK`, which is supported since 4.12 kernel.
	//
	// If not supported, `unix.ENOPROTOOPT` is returned.

	err = rtnl.SetOption(netlink.ExtendedAcknowledge, true)
	if err != nil {
		log.Printf("could not set option ExtendedAcknowledge: %v\n", err)
		return false
	}

	// get all the qdiscs from all interfaces
	qdiscs, err := rtnl.Qdisc().Get()
	if err != nil {
		log.Printf("could not get qdiscs: %v\n", err)
		return false
	}

	for _, qdisc := range qdiscs {
		iface, err := net.InterfaceByIndex(int(qdisc.Ifindex))
		if err != nil {
			log.Printf("could not get interface from id %d: %v", qdisc.Ifindex, err)
			continue
		}
		if iface.Index == netInterface.Index {
			if qdisc.Kind == "clsact" {
				return true
			}
		}
	}
	return false
}
