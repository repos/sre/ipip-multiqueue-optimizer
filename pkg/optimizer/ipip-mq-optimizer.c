//go:build ignore
/*
 * SPDX-License-Identifier: GPL-2.0
 * Copyright 2023-present Valentin Gutierrez
 * Copyright 2023-present Wikimedia Foundation, Inc.
*/
#include <linux/bpf.h>
#include <linux/if_ether.h>
#include <linux/in.h>
#include <linux/ip.h>
#include <linux/ipv6.h>
#include <linux/pkt_cls.h>
#include <linux/tcp.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>

#include <stddef.h>
// RFC1918: we are going to use 172.16/10 as our src (4268 is 172.16 in BE)
// for ipip header
#define IPIP_V4_PREFIX 4268
// RFC 6666: we are going to use 0100::/64 discard prefix as our src
// for ip(6)ip6 header
#define IPIP_V6_PREFIX1 1
#define IPIP_V6_PREFIX2 0
#define IPIP_V6_PREFIX3 0

#define IP4_PROTO_OFF (ETH_HLEN + offsetof(struct iphdr, protocol))
#define IP4_CSUM_OFF (ETH_HLEN + offsetof(struct iphdr, check))
#define IP4_SRC_OFF (ETH_HLEN + offsetof(struct iphdr, saddr))
#define IP6_SRC_OFF (ETH_HLEN + offsetof(struct ipv6hdr, saddr))
#define TCP4_SPORT_OFF (ETH_HLEN + sizeof(struct iphdr) + sizeof(struct iphdr) + offsetof(struct tcphdr, source))
#define TCP6_SPORT_OFF (ETH_HLEN + sizeof(struct ipv6hdr) + sizeof(struct ipv6hdr) + offsetof(struct tcphdr, source))
#define IP6_NH_OFF (ETH_HLEN + offsetof(struct ipv6hdr, nexthdr))

#define STATS_INDEX 0
#define STATS_SIZE 1
#define NO_FLAGS 0


struct op_stats {
    __u64 pckts_processed;
    __u64 pckts_skipped;
};

struct {
    __uint(type, BPF_MAP_TYPE_PERCPU_ARRAY);
    __type(key, __u32);
    __type(value, struct op_stats);
    __uint(max_entries, STATS_SIZE);
    __uint(map_flags, NO_FLAGS);
} op_stats_map SEC(".maps");


// Probably missing an include
// source: https://mozillazg.com/2022/07/ebpf-libbpf-what-is-load_byte-load_half-load_word-en.html
unsigned long long load_byte(void *skb, unsigned long long offset) asm("llvm.bpf.load.byte");
unsigned long long load_half(void *skb, unsigned long long offset) asm("llvm.bpf.load.half");
unsigned long long load_word(void *skb, unsigned long long offset) asm("llvm.bpf.load.word");


static inline void set_ipv4_src(struct __sk_buff *skb,__u32 old_ip,  __u32 new_ip)
{
    bpf_l3_csum_replace(skb, IP4_CSUM_OFF, old_ip, new_ip, sizeof(new_ip));
    bpf_skb_store_bytes(skb, IP4_SRC_OFF, &new_ip, sizeof(new_ip), 0);
}

SEC("tc")
int ipip_optimizer(struct __sk_buff* skb) {
    __u32 ip_suffix;
    __u32 stats_key = STATS_INDEX;
    struct op_stats* stats;
    stats = bpf_map_lookup_elem(&op_stats_map, &stats_key);
    if (!stats) {
        return TC_ACT_UNSPEC;
    }

    __u16 h_proto = load_half(skb, offsetof(struct ethhdr, h_proto));
    switch (h_proto) {
        case ETH_P_IP:
            // Skip non IPIP packets
            if (load_byte(skb, IP4_PROTO_OFF) != IPPROTO_IPIP) {
                stats->pckts_skipped += 1;
                return TC_ACT_UNSPEC;
            }
            // source IP randomization stolen from
            // https://github.com/facebookincubator/katran/blob/fdabc8d9473d633b4aae9e35b6f97c9a9465e1a2/katran/lib/bpf/pckt_encap.h#L105-L107
            ip_suffix = bpf_htons(load_half(skb, TCP4_SPORT_OFF));
            __u32 old_ip = bpf_htonl(load_word(skb, IP4_SRC_OFF));

            ip_suffix <<= 16;
            ip_suffix ^= old_ip;
            set_ipv4_src(skb, old_ip, ((0xFFFF0000 & ip_suffix) | IPIP_V4_PREFIX));
            stats->pckts_processed += 1;
            return TC_ACT_OK;
        case ETH_P_IPV6:
            // Skip non IPv6/IPv6 packets
            if (load_byte(skb, IP6_NH_OFF) != IPPROTO_IPV6) {
                stats->pckts_skipped += 1;
                return TC_ACT_UNSPEC;
            }
            __u32 saddr[4];
            ip_suffix = load_word(skb, IP6_SRC_OFF+(sizeof(__be32)*3)) ^ bpf_htons(load_half(skb, TCP6_SPORT_OFF));
            saddr[0] = IPIP_V6_PREFIX1;
            saddr[1] = IPIP_V6_PREFIX2;
            saddr[2] = IPIP_V6_PREFIX3;
            saddr[3] = ip_suffix;
            bpf_skb_store_bytes(skb, IP6_SRC_OFF, &saddr, sizeof(saddr), 0);
            stats->pckts_processed += 1;
            return TC_ACT_OK;
        default:
            stats->pckts_skipped += 1;
            return TC_ACT_UNSPEC;
    }
}
char _license[] SEC("license") = "GPL";
