module gitlab.wikimedia.org/repos/sre/ipip-multiqueue-optimizer

go 1.19

require (
	github.com/cilium/ebpf v0.11.0
	github.com/florianl/go-tc v0.4.2
	github.com/mdlayher/netlink v1.7.2
	github.com/mmatczuk/anyflag v0.0.0-20231026075539-5f42d2f36d96
	github.com/prometheus/client_golang v1.17.0
	github.com/spf13/cobra v1.7.0
	golang.org/x/sys v0.11.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/josharian/native v1.1.0 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.4 // indirect
	github.com/mdlayher/socket v0.4.1 // indirect
	github.com/prometheus/client_model v0.4.1-0.20230718164431-9a2bf3000d16 // indirect
	github.com/prometheus/common v0.44.0 // indirect
	github.com/prometheus/procfs v0.11.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/exp v0.0.0-20230224173230-c95f2b4c22f2 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sync v0.3.0 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
)
