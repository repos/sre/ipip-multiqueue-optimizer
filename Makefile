BINARY_NAME = ipip-multiqueue-optimizer
GO_GENERATE = go generate
GO_BUILD = go build
GOARCH ?= amd64
GOOS ?= linux
CGO_ENABLED ?= 0

all: build

clean:
	go clean
	rm -f $(BINARY_NAME)

generate:
	$(GO_GENERATE) ./...

build: generate
	CGO_ENABLED=$(CGO_ENABLED) GOARCH=$(GOARCH) GOOS=$(GOOS) $(GO_BUILD) -o $(BINARY_NAME) .

run: dep build
	./$(BINARY_NAME)

dep:
	go mod download

test:
	go test .
